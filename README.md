# Nonlinear Dynamics Tools

The application of nonlinear dynamics tools for analyzing dynamic data using Python.

This is developed specificially for combustor data analysis.

## Functions

1. dimension -> calculated dimension of the dynamic data
2. rqa -> create recurrence plot from dynamic data
3. systems -> different dynamic systems
    A) Rossler System
    B) Lorenz System
    these can be utilized to test the dynamic analysis codes and compare with standard results.

## Usage


## References


