"""
*******************************************************************************
              METHODS TO CALCULATE MINIMUM EMBEDDING DIMENSION (d)
...............................................................................
DESCRIPTION:
    1. Method 1: FNN
       > Eliminating the false crossing of the phase space trajectories [1]
       > Threshold is chosen between 10 to 50 [2] (limitation)
       > For noisy signal [2] suggests extra criterion of flase neighbors

    2. Method 2: Cao
       > Threshold dependence on the embedding dimension is the limitation of
         FNN methods
       > E1(d) will stop changing once all the false neighbors are resolved
         after a dimension d0 . Then, d0 + 1 can be chosen as the minimum
         embedding dimension required for the construction of the phase space
       > E2(d) is defined for distinguishing deterministic signal from a
         stochastic signal
       > E2(d) = 1 for  every d, for random signals
         E2(d) != 1 for initial d values for deterministic signals
...............................................................................
REFERENCES:
    1. M. B. Kennel, R. Brown, and H. D. I. Abarbanel, “Determining embedding
    dimension for phase-space reconstruction using a geometrical construction,”
    Physical Review A, vol. 45, no. 6, Art. no. 6, Mar. 1992

    2. H. D. Abarbanel, R. Brown, J. J. Sidorowich, and L. S. Tsimring,
    “The analysis of observed chaotic data in physical systems,” Reviews of
    modern physics, vol. 65, no. 4, p. 1331, 1993

    3. L. Cao, “Practical method for determining the minimum embedding
    dimension of a scalar time series,” Physica D: Nonlinear Phenomena,
    vol. 110, no. 1-2, Art. no. 1–2, Dec. 1997

    4. V. Nair, G. Thampi, S. Karuppusamy, S. Gopalan, and R. Sujith, “Loss of
    chaos in combustion noise as a precursor of impending combustion
    instability,” International journal of spray and combustion dynamics,
    vol. 5, no. 4, pp. 273–290, 2003

    5. J. Tony, E. Gopalakrishnan, E. Sreelekha, and R. Sujith, “Detecting
    deterministic nature of pressure measurements from a turbulent combustor,”
    Physical Review E, vol. 92, no. 6, p. 062902, 2015.

...............................................................................
DEVELOPER: NISANTH M. S, m.nisanth.s@gmail.com
*******************************************************************************
"""
#-------------------------------------------------[Imports]--------------------
import numpy as np
import numpy.matlib
import vector
#------------------------------------------------------------------------------

#---------------------------------------------[Variables and Units]------------
# embedding dimension (d)     [number] maximu value = dmax
# delay time (tau)            [number] tau*(Fs)*1e3 = tau_ms  [milli-seconds]
#------------------------------------------------------------------------------

#---------------------------------------------[FNN Function]-------------------
def FNN(x, tau, dmax, Rtol=10, Atol=2):
    print("\nFNN Running .....")

    x = x - np.mean(x)
    N = len(x)
    FN = np.zeros(dmax)
    dm = np.arange(1,dmax+1)

    for k in np.arange(1,dmax+1):
        N1 = N - (k)*tau
        Y = vector.delayVector(x, tau, k, N1)

        for j in np.arange(0,N1):
            Y0 = np.ones((N1,1))
            Y1 = Y0*Y[j,:]
            R = np.sqrt(np.sum ((Y-Y1)**2, axis=1))
            b = np.argsort(R) # index of sorted array (issue ! : equal values)
            a = R[b] # array value corresponding to indices
            NN, ND = b[1], a[1]
            Rd = x[j+(k)* tau] - x[NN+(k)* tau]
            Rd1 = np.sqrt(Rd**2+ ND**2)

            if abs(Rd)/ND > Rtol:
                FN[k-1] = FN[k-1] + 1
            elif Rd1/np.std(x, ddof=1) > Atol: # ddof=1 for matlab std
                FN[k-1] = FN[k-1] + 1

    # print('\nFNN = ', FN)
    FN = (FN/FN[0])*100

    return {'d':dm , 'FNN':FN}
#------------------------------------------------------------------------------

#---------------------------------------------[Cao Function]-------------------
def cao(x, tau, dmax):
    print("\nCao Running .....")

    E_m =[]
    Estar_m = []
    M = []

    for m in np.arange(1,dmax+2): # changed to +1 instead of +2
        n = int(len(x)/tau)*tau
        Z= []
        for i in np.arange(0,m+1):
            # Y[:,i] = x[i*tau : n-(m-i)*tau]
            Z.append(np.array(x[i*tau : n-(m-i)*tau]))
        Y = np.array(Z).transpose()
        N = len(Y[:,0])
        dist = np.zeros(N)
        a2 = []

        for i in np.arange(0,N):

            temp = list(Y[i,:])  # Swapping of Y vector after each iteration
            Y[i,:] = list(Y[0,:]) # assignment operation will not work with numpy
            Y[0,:] = temp

            Rd = (np.sqrt(np.sum((np.matlib.repmat(Y[0,0:-1],len(Y[:,0])-1,1) \
                                  - Y[1:,0:-1])**2,axis=1))).transpose()

            pos_mRd = np.where(Rd == min(Rd))[0]
            val_mRd = Rd[pos_mRd][0] # modfied , taken only one minimum valus
            # dist.append(abs(Y[0,-1] - Y[1+pos_mRd[0],-1]))
            dist[i] = abs(Y[0,-1] - Y[1+pos_mRd[0],-1])

            val_mRd = val_mRd + 1e-12 # remove divide by zero
            minRdplus1 = np.sqrt(val_mRd*val_mRd + dist[i]*dist[i]) + 1e-12
            a2.append(minRdplus1/val_mRd)

        E_m.append(np.mean(np.array(a2)))
        Estar_m.append(np.mean(np.array(dist)))
        M.append(m)

    E1 = np.array(E_m[1:])/np.array(E_m[0:-1])
    E2 = np.array(Estar_m[1:])/np.array(Estar_m[0:-1])

    return {'d':M[:-1], 'E1':E1, 'E2':E2}
#------------------------------------------------------------------------------


#---------------------------------------------[Issues]-------------------------
# 1. Calculation of std requred attension, for matlab method np.std(x, ddof=1)

# 2. Sorting in FNN method, if equal values are coming in the list, matlab
#    and numpy sort it in different way, implies reflected in the condition
#    stateement
