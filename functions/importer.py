"""
*******************************************************************************
            DATA IMPORTERS FOR PRESSURE-TIME SERIES ANALYSIS
...............................................................................
DESCRIPTION:
    1. Importing the data to dictionary files
...............................................................................
REFERENCES:
    1. 
...............................................................................
DEVELOPER: NISANTH M. S, m.nisanth.s@gmail.com
*******************************************************************************
"""
#-------------------------------------------------[Imports]--------------------
import csv
import numpy as np
#------------------------------------------------------------------------------

#---------------------------------------------[Variables and Units]------------
# embedding dimension (d)     [number]
#------------------------------------------------------------------------------

#---------------------------------------------[CSV Data Reading]---------------
def readData(fileAddress,Frequency):
    with open(fileAddress) as file:
        data = csv.reader(file, delimiter = ',')
        I = []
        T = []
        A = []
        count = 1
        time = 0
        for row in data:
            I.append(count)
            T.append(time)
            A.append(float(row[0]))
            count +=1
            time +=1/Frequency
        data = {'index':np.array(I), 'time':np.array(T), 'data':np.array(A)}
    return data
#------------------------------------------------------------------------------

#---------------------------------------------[Issues/Comments]----------------
# 

