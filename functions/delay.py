"""
*******************************************************************************
                 METHODS TO CALCULATE OPTIMUM TIME DELAY (TAU)
...............................................................................
DESCRIPTION:
    1. Method 1: ACF
       > It is based on the linear theory, so it detects only linear
         independence of the coordinates [1,2,3]
       > Zero crossing value of tau is the optimum time delay

    2. Method 2: AMI
       > This method is recommended for nonlinear systems, and it is based
         on the information theory [4]
       > AMI is generalized version of autocorrelation function
       > It measures the extent to which x(t+tau) is related to x(t) at
         given tau
       > The first local minima of AMI is chosen as the optimum time delay [3]
...............................................................................
REFERENCES:
    1. A. H. Nayfeh and B. Balachandran, Applied nonlinear dynamics:
    analytical, computational and experimental methods.John Wiley & Sons, 2008

    2. H. G. Schuster and W. Just, Deterministic chaos: an introduction.
    John Wiley & Sons, 2006.

    3. H. D. Abarbanel, R. Brown, J. J. Sidorowich, and L. S. Tsimring,
    “The analysis of observed chaotic data in physical systems,” Reviews of
    modern physics, vol. 65, no. 4, p. 1331, 1993

    4. A. M. Fraser and H. L. Swinney, “Independent coordinates for strange
    attractors from mutual information,” Physical review A, vol. 33, no. 2,
    p. 1134, 1986
...............................................................................
DEVELOPER: NISANTH M. S, m.nisanth.s@gmail.com
*******************************************************************************
"""
#-------------------------------------------------[Imports]--------------------
import math
import numpy as np
#------------------------------------------------------------------------------

#---------------------------------------------[Variables and Units]------------
# maximum delay time (tau_max) [number] tau*(Fs)*1e3 = tau_ms  [milli-seconds]
#------------------------------------------------------------------------------

#---------------------------------------------[ACF Function]-------------------
def ACF(x, tau_max):
    print("\nACF for time delay ......")

    N = len(x)
    xMean = np.mean(x)
    rk = []
    k1 = []

    for k in range(0,tau_max):
        top = 0
        bottom = 0

        for i in range(0, N-k):
            top = top + ((x[i] - xMean)*(x[i+k] - xMean)) #numerator
            bottom = bottom + ((x[i] - xMean)**2) #denominator

        r = top/bottom #function rk

        rk.append(r)
        k1.append(k)

    return {'tau':np.array(k1), 'rk':np.array(rk)}
#------------------------------------------------------------------------------

#---------------------------------------------[AMI Function]-------------------
def AMI(signal, lag):
    print("\nAMI for time delay ......")

    x = np.array(signal)
    y = np.array(signal)
    N = len(x)
    L = np.arange(0,lag+1)
    x = x - min(x)
    x = x*(1-np.spacing(1))/max(x)
    y = y - min(y)
    y = y*(1-np.spacing(1))/max(y)
    v = np.zeros(len(L))
    w = np.zeros(len(L))

    for i in np.array(L):
        bins = math.ceil(math.log2(N-L[i]))
        binx = (np.floor(x*bins) + 1).astype(int) # integer bins
        biny = (np.floor(y*bins) + 1).astype(int) # integer bins
        Pxy = np.zeros((bins,bins))

        for j in np.arange(0,(N)-L[i]):
            k = j + L[i];
            Pxy[binx[k]-1, biny[j]-1] = Pxy[binx[k]-1, biny[j]-1] + 1

        Pxy = Pxy /(N - L[i])
        Pxy = Pxy + np.spacing(1)
        Px = np.sum (Pxy ,axis=1)
        Py = np.sum (Pxy ,axis=0)
        q = Pxy/(Px.reshape(-1,1)*Py)
        MI = Pxy * np.log2(q)
        v[i] = np.sum(MI)

        # Shannon entropy can be calculated from following equations
        Hx = -np.sum(Px*np.log2(Px)) # element wise multiplication
        Hy = -np.sum(Py*np.log2(Py))
        Hxy = -np.sum(Pxy*np.log2(Pxy))
        w[i] = Hx + Hy - Hxy

    return {'tau':np.array(L), 'AMI':np.array(v), 'Entropy':np.array(w)}
#------------------------------------------------------------------------------

#---------------------------------------------[Issues/Comments]----------------
# 1.
