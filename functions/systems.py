"""
*******************************************************************************
                        STANDARD DYNAMICAL SYSTEMS
...............................................................................
DESCRIPTION:
    1. Rossler System
...............................................................................
REFERENCES:
    1. 
...............................................................................
DEVELOPER: NISANTH M. S, m.nisanth.s@gmail.com
*******************************************************************************
"""
##------------------------------------------------[Imports]--------------------
import numpy as np
##-----------------------------------------------------------------------------

##--------------------------------------------[Variables and Units]------------
# embedding dimension (d)     [number]
##-----------------------------------------------------------------------------

##--------------------------------------------[Rossler System]-----------------
def rossler(dt, length, start, a=0.2, b=0.2, c=0.78):
    """
    Rossler [1976] Chaotic system model.
        xdot = - y - z
        ydot = x + a * y
        zdot = b + x * z - c * z

    Parameters
    ----------
    dt : float
        Time of measurement or observation (1/freqency).
    length : int
        Number of data points need to be measured.
    start : list
        Initial condition of the problem.
    a : float, optional
        Parameter a. The default is 0.2.
    b : float, optional
        Parameter b. The default is 0.2.
    c : float, optional
        Parameter c. The default is 0.78.

    Returns
    -------
    list
        List of phase space coordinates [x[i],y[i],z[i]].

    """
    xv = np.empty(length+1)
    yv = np.empty(length+1)
    zv = np.empty(length+1)
    
    xv[0] = start[0]
    yv[0] = start[1]
    zv[0] = start[2]
    
    for i in range(length):
        xdot = - yv[i] - zv[i]
        ydot = xv[i] + a * yv[i]
        zdot = b + xv[i] * zv[i] - c * zv[i]
        # print(xv[i], yv[i], zv[i])
        # print(xdot, ydot, zdot)
        
        xv[i+1] = xv[i] + xdot*dt
        yv[i+1] = yv[i] + ydot*dt
        zv[i+1] = zv[i] + zdot*dt

    return [xv[:-1], yv[:-1], zv[:-1]]
##-----------------------------------------------------------------------------

##--------------------------------------------[Lorenz System]-----------------
def lorenz(dt, length, start, s=10, r=28, b=8/3):
    """
    Lorenz [1963] Chaotic system model.
        xdot = s * (y - x)
        ydot = -x * z + r * x - y
        zdot = x * y - b * z

    Parameters
    ----------
    dt : float
        Time of measurement or observation (1/freqency).
    length : int
        Number of data points need to be measured.
    start : list
        Initial condition of the problem.
    s : float, optional
        Parameter a. The default is 10.
    r : float, optional
        Parameter b. The default is 28.
    b : float, optional
        Parameter c. The default is 8/3.

    Returns
    -------
    list
        List of phase space coordinates [x[i],y[i],z[i]].

    """
    xv = np.empty(length+1)
    yv = np.empty(length+1)
    zv = np.empty(length+1)
    
    xv[0] = start[0]
    yv[0] = start[1]
    zv[0] = start[2]
    
    for i in range(length):
        xdot = s * (yv[i] - xv[i])
        ydot = -xv[i] * zv[i] + r * xv[i] - yv[i]
        zdot = xv[i] * yv[i] - b * zv[i]
        # print(xv[i], yv[i], zv[i])
        # print(xdot, ydot, zdot)
        
        xv[i+1] = xv[i] + xdot*dt
        yv[i+1] = yv[i] + ydot*dt
        zv[i+1] = zv[i] + zdot*dt

    return [xv[:-1], yv[:-1], zv[:-1]]
##-----------------------------------------------------------------------------

##--------------------------------------------[Issues/Comments]----------------
# 

