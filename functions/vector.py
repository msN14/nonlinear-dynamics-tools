"""
*******************************************************************************
            CALCULATE THE DELAY VECTOR FOR TIME SERIES DATA (VECTOR)
...............................................................................
DESCRIPTION:
    1. Delay vector Z is formated from time series data x(i) using
       time delay (tau) and embedding dimension (d)

 Zi = [x(i),x(i+tau),x(i+2*tau),...,x(i+(d-1)*tau)] and
 i = 1,2,3,...,N-(d-1)*tau
...............................................................................
REFERENCES:
    1. F. Takens, “Detecting strange attractors in turbulence,” in Lecture 
       Notes in Mathematics, Springer Berlin Heidelberg, 1981, pp. 366–381.
...............................................................................
DEVELOPER: NISANTH M. S, m.nisanth.s@gmail.com
*******************************************************************************
"""
#-------------------------------------------------[Imports]--------------------
import numpy as np
#------------------------------------------------------------------------------

#---------------------------------------------[Variables and Units]------------
# embedding dimension (d)     [number]
# delay time (tau)            [number] tau*(Fs)*1e3 = tau_ms  [milli-seconds]
#------------------------------------------------------------------------------

#---------------------------------------------[Delay Vector Function]----------
def delayVector(x, tau, d, N1=None):
    print("\nDelay Vector ......")
    N = len(x) # Length of the data
    
    if N1==None:
        Length = N - (d-1)*tau # Length of Zi array
    else:
        Length = N1 # Option added for the use in FNN method (d+1) vector (?)
        
    Y = [] # list initializing
    # print("Delay Vector = ", Length)
    for j in np.arange(0,d): # proper my method as per definition
    # for j in np.arange(d-1,-1,-1): # reversed arrangement in matlab code
        Y.append(np.array(x[j*tau : Length + j*tau]))
    Z = np.array(Y).transpose()
    
    return Z # numpy array of delay vector
#------------------------------------------------------------------------------

#---------------------------------------------[Issues/Comments]----------------
# 1. Representation of N1 = N - k*tau for FNN application ?
# 2. Reverese arrangement of delay vector in matlab code ?

# 3. Array is validated with avilable matlab code

