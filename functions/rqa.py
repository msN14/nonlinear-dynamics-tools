"""
*******************************************************************************
               RECURRENCE QUANTIFICATION ANALYSIS (RQA)
...............................................................................
DESCRIPTION:
    1. Includes a set of functions to calculate quantities from RP matrix
...............................................................................
REFERENCES:
    1.
...............................................................................
DEVELOPER: NISANTH M. S, m.nisanth.s@gmail.com
*******************************************************************************
"""
##------------------------------------------------[Imports]--------------------
import numpy as np
##-----------------------------------------------------------------------------

##--------------------------------------------[Variables and Units]------------
# embedding dimension (d)     [number]
##-----------------------------------------------------------------------------

##--------------------------------------------[Number of Black Lines]----------
def P(length, Rmatrix):
    l = length
    # Rmatrix = Rmatrix.astype('int') # boolian to int conversion
    R = np.rot90(Rmatrix, -1) # rotate clockwise direction 90 degree
    N1 = len(R[0])
    lineNumber = 0 # initialization
    for i in np.arange(1,N1-l):
        for j in np.arange(1,N1-l):
            A = (1-R[i-1,j-1])*(1-R[i+l,j+l]) # white ends
            B = 1 # initialization
            for k in np.arange(0,l):
                B = B * R[i+k,j+k] # black dots
            lineNumber = lineNumber + A * B
    return lineNumber
##-----------------------------------------------------------------------------

##--------------------------------------------[Number of Black Lines]----------
def Ptau(diagonal, length, Rmatrix):
    tau = diagonal # tau = 0 : main diagonal
    l = length
    R = Rmatrix
    N1 = len(R[0])
    C, S, E = 0, 1, 1 # initialization
    for i in np.arange(1, N1-l-tau):
        A = (1 - R[i-1, i-1+tau]) * (1 - R[i+l, i+l+tau])
        B = 1 # initialization
        for k in np.arange(0,l):
            B = B * R[i+k, i+k+tau] # intermediate blackline
            if i == 1 : # considering starting blackline
                S = S * R[i-1+k, i-1+tau+k] * (1 - R[i-1+l,i-1+tau+l])
            if i == N1-l-tau-1: #considering ending blackline
                E = E * R[i+k+1, i+tau+k+1] * (1 - R[i,i+tau])
        C = C + A * B
    Cinner = C
    C = C + S + E
    return C
    # problem with last diagonal lines, will not take the values
##-----------------------------------------------------------------------------

##--------------------------------------------[Number of Black Lines]----------
def PtauAlt(diagonal, length, Rmatrix):
    # print("PtauAlt at diagonal = ", diagonal,"....")
    D = np.diagonal(Rmatrix, diagonal)
    D = np.append(np.append(0,D),0)
    l = length
    # print(D)
    b, c = 0, 0
    for i in np.arange(0,len(D)-l-1):
        a = (1-D[i])*(1-D[i+l+1])
        b=1
        for k in np.arange(0,l):
            b = b * D[i+1+k]
        c = c + a * b
    # print(c)
    return c
    # Alternate method works perfectly ! # Tested
##-----------------------------------------------------------------------------

##--------------------------------------------[Black line length array]--------
def BLA(diagonal, Rmatrix):
    D = np.diagonal(Rmatrix, diagonal)
    D = np.append(np.append(0,D),0)
    
    # Creating black line length array
    # A = []
    B = []
    c = 0
    iprev=0
    for i,d in enumerate(D):
        if i!=0:
            if d==1 and i==iprev+1:
                # A.append(d)
                c = c + 1
            if D[iprev]==1 and d==0:
                # A.append(0)
                B.append(c)
                c = 0
        else:
            if d==1:
                # A.append(d)
                c = c + 1
        iprev = i
        
    # Processing length array and making dictionary
    unique, counts = np.unique(np.array(B), return_counts=True)
    blcd = dict(zip(unique, counts))
    return blcd # {length : count}
##-----------------------------------------------------------------------------

# tau-recurrence rate index (RR_tau)
##--------------------------------------------[RRtau]--------------------------
def RRtau(Rmatrix):
    print('RRtau Calculating ......')
    N1 = len(Rmatrix[0])
    Tau = []
    RR = []
    for tau in np.arange(0, N1):
        r = 0
        for l in np.arange(1, N1-tau+1):
            r = r + l * PtauAlt(tau, l, Rmatrix)
        R = (1/(N1-tau)) * r
        Tau.append(tau)
        RR.append(R)
        print('tau = ', tau)
    return({'tau':np.array(Tau), 'RRtau':np.array(RR)})
    # Tested with matrix -> working fine
##-----------------------------------------------------------------------------

##--------------------------------------------[RRtau Fast]---------------------
def RRtauFast(Rmatrix):
    print('RRtau Fast Calculating ......')
    N1 = len(Rmatrix[0])
    Tau = []
    RR = []
    for tau in np.arange(0, N1):
        r = np.sum(np.diagonal(Rmatrix, tau))
        R = (1/(N1-tau)) * r
        Tau.append(tau)
        RR.append(R)
        # print('tau = ', tau, 'r=', r)
    return({'tau':np.array(Tau), 'RRtau':np.array(RR)})
    # Tested with matrix -> working fine
##-----------------------------------------------------------------------------

# tau-determinism index (DET_tau)
##--------------------------------------------[DETtau]-------------------------
def DETtau(lmin, Rmatrix):
    print('\nDETtau Slow Calculating ......')
    N1 = len(Rmatrix[0])
    Tau = []
    DET = []
    for tau in np.arange(0, N1):
        print("tau = ", tau)
        n = 0
        d = 0
        for l in np.arange(lmin, N1-tau+1):
            print("l=",l)
            n = n + l * PtauAlt(tau, l, Rmatrix)
        for l in np.arange(l, N1-tau+1):
            d = d + l * PtauAlt(tau, l, Rmatrix)
        det = n/d
        Tau.append(tau)
        DET.append(det)
    return({'tau':np.array(Tau), 'DETtau':np.array(DET)})
##-----------------------------------------------------------------------------

##--------------------------------------------[DETtau Test]--------------------
def DETtauTest(lmin, Rmatrix):
    print('\nDETtau Test Calculating ......')
    N1 = len(Rmatrix[0])
    Tau = []
    DET = []
    for tau in np.arange(0, N1):
        print("tau = ", tau)
        n1 = 0
        n = 0
        d = 0
        for l in np.arange(1, lmin+1):
            print("l=", l)
            P = PtauAlt(tau, l, Rmatrix)
            n1 = n1 + l * P
        d = np.sum(np.diagonal(Rmatrix, tau))
        det = (n-n1)/d
        Tau.append(tau)
        DET.append(det)
    return({'tau':np.array(Tau), 'DETtau':np.array(DET)})
##-----------------------------------------------------------------------------

##--------------------------------------------[DETtau Fast]--------------------
def DETtauFast(lmin, Rmatrix):
    print('\nDETtau Fast Calculating ......')
    N1 = len(Rmatrix[0])
    Tau = []
    DET = []
    for tau in np.arange(0, N1):
        print("tau = ", tau)
        n = 0
        d = 0
        blcd = BLA(tau, Rmatrix)
        for key, value in blcd.items():
            if key >= lmin:
                n = n + key*value
                d = d + key*value
            else:
                d = d + key*value
        if d!=0:
            det = n/d
        else:
            det = 0
        Tau.append(tau)
        DET.append(det)
    return({'tau':np.array(Tau), 'DETtau':np.array(DET)})
##-----------------------------------------------------------------------------

# tau-average diagonal line length index (L_tau)
##--------------------------------------------[Ltau]---------------------------
def Ltau(lmin, Rmatrix):
    N1 = len(Rmatrix[0])
    Tau = []
    L = []
    for tau in np.arange(0, N1):
        n = 0
        d = 0
        for l in np.arange(lmin, N1-tau+1):
            n = n + l * PtauAlt(tau, l, Rmatrix)
        for l in np.arange(lmin, N1-tau+1):
            d = d + PtauAlt(tau, l, Rmatrix)
        ltau = n/d
        Tau.append(tau)
        L.append(ltau)
    return({'tau':np.array(Tau), 'Ltau':np.array(L)})
##-----------------------------------------------------------------------------

##--------------------------------------------[LtauFast]-----------------------
def LtauFast(lmin, Rmatrix):
    N1 = len(Rmatrix[0])
    Tau = []
    L = []
    for tau in np.arange(0, N1):
        print("tau = ", tau)
        n = 0
        d = 0
        blcd = BLA(tau, Rmatrix)
        for key, value in blcd.items():
            if key >= lmin:
                n = n + key*value
                d = d + value
        if d!=0:
            ltau = n/d
        else:
            ltau = 0
        Tau.append(tau)
        L.append(ltau)
    return({'tau':np.array(Tau), 'Ltau':np.array(L)})
##-----------------------------------------------------------------------------

# tau-entropy index (S_tau)
##--------------------------------------------[Stau]---------------------------
def Stau(lmin, Rmatrix):
    N1 = len(Rmatrix[0])
    Tau = []
    S = []
    for tau in np.arange(0, N1):
        s = 0
        Ntotal = TotalLineNum(tau, Rmatrix) # calulate number of lines in tau
        for l in np.arange(lmin, N1-tau+1):
            Pl = PtauAlt(tau, l, Rmatrix)
            probability = Pl/Ntotal # clarification required, Shannon Entropy
            s = s + probability * np.ln(probability)
        Tau.append(tau)
        S.append(-1 * s)
    return({'tau':np.array(Tau), 'Stau':np.array(S)})
##-----------------------------------------------------------------------------

##--------------------------------------------[Stau Fast]----------------------
def StauFast(lmin, Rmatrix):
    N1 = len(Rmatrix[0])
    Tau = []
    S = []
    for tau in np.arange(0, N1):
        print("tau = ", tau)
        n = 0
        s = 0
        Ntau = 0
        blcd = BLA(tau, Rmatrix)
        
        for key, value in blcd.items():
            Ntau = Ntau + value
            
        for key, value in blcd.items():
            if key >= lmin:
                n = n + value
        if Ntau > 0:
            probability = n/Ntau
        else:
            probability = 0
        
        s = s + probability * np.log(probability)
        
        Tau.append(tau)
        S.append(-1 * s)
    return({'tau':np.array(Tau), 'Stau':np.array(S)})
##-----------------------------------------------------------------------------

# Calcuate the number of lines (all lengths) in a specific diagonal tau
##--------------------------------------------[Total Number of Lines]----------
def TotalLineNum(tau, Rmatrix):
    N1 = len(Rmatrix[0])
    NL = 0
    for l in np.arange(1,N1-tau+1):
        NL = NL + PtauAlt(tau, l, Rmatrix)
    return(NL)
##-----------------------------------------------------------------------------




##--------------------------------------------[Issues/Comments]----------------
#
# To Calculate the area of recurrencePlot
# import numpy as np

def RR_area(boolMatrix):
    black = np.sum(boolMatrix)
    N1 = len(boolMatrix[0])
    area_ratio = black/(N1**2)
    return (area_ratio)
