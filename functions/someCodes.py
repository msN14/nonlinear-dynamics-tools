
# %% Dark Line Length Array
##--------------------------------------------[Dark line length Array]---------

# D = [0,1,1,0,1,1,1,0,1,0,0,0,1,1,0]

import numpy as np

# D = np.zeros(3,int)
# D[0] = 0
# D[1] = 1
# D[2] = 1

D = [1,0,1,1,1,0,1,1,1,1,1,0]
D = np.append(np.append(0,D),0)

A = []
B = []

c = 0
iprev=0

for i,d in enumerate(D):
    if i!=0:
        if d==1 and i==iprev+1:
            A.append(d)
            c = c + 1
        if D[iprev]==1 and d==0:
            A.append(0)
            B.append(c)
            c = 0
    else:
        if d==1:
            A.append(d)
            c = c + 1
    iprev = i
    
print("length array = ", B)
        
# %%
