"""
*******************************************************************************
                  DETERMINISM OF THE TIME SERIES DATA
...............................................................................
DESCRIPTION:
    1. 
...............................................................................
REFERENCES:
    1. http://arxiv.org/pdf/0906.1418v1.  Paul Matthews, July 2009
    2. https://www.mathworks.com/help/stats/corr.html
...............................................................................
DEVELOPER: NISANTH M. S, m.nisanth.s@gmail.com
*******************************************************************************
"""
##------------------------------------------------[Imports]--------------------
import numpy as np
import vector
from scipy.stats import linregress
#------------------------------------------------------------------------------

##--------------------------------------------[Variables and Units]------------
# embedding dimension (d)     [number]
#------------------------------------------------------------------------------

##--------------------------------------------[0-1 Test]-----------------------
def zeroOneTest(x):
    """
    0-1 Test to evaluvate determinism of time series data.

    Parameters
    ----------
    x : numpy.darray
        Singnal data array.

    Returns
    -------
    Kmedian : float
        Kmedian.

    """
    N = len(x)
    j = np.arange(1,N+1)
    t = np.arange(1,round(N/10)+1)
    M = np.zeros(round(N/10))
    # 100 random c values in [pi/5,4pi/5]
    c = np.pi/5 + np.random.random_sample((100,))*3*np.pi/5
    Kcorr = []
    
    for its in np.arange(1,100):
        p = np.cumsum(x*np.cos(j*c[its]))
        q = np.cumsum(x*np.sin(j*c[its]))
        
        for n in np.arange(0, round(N/10)):
            A = (p[n+1:N+1]-p[0:N-n-1])**2
            B = (q[n+1:N+1]-q[0:N-n-1])**2
            C = (1 - np.cos(n*c[its]))
            D = (1 - np.cos(c[its]))
            
            M[n] = np.mean(A + B) - ((np.mean(x)**2)*C)/D
        
        # print('t = ', len(t))
        # print('M = ', len(M))
        Kcorr.append(np.corrcoef(t, M))
        
    Kmedian = np.median(Kcorr)
    return Kmedian
#------------------------------------------------------------------------------

##--------------------------------------------[Cm(R) First Code]---------------
def Csum(x, d, tau, Rs):
    Rs = np.array(Rs)
    Dv  = vector.delayVector(x, tau, d)
    Nv = len(Dv)
    Nc = int(0.1*Nv)
    CR = []
    
    for R in Rs:
        p = []
        print("\nR = ", R , " .....")
        # Selecting random centers
        iChoice = np.random.choice(np.arange(0,Nv), Nc, replace=False)
        Nb = 0
        for i in iChoice:
            # R distance around center i
            if (max(np.abs(Dv[i])) + R) < 1.0: # box inside the embedded space
                # print("Dv[i]=", Dv[i], max(np.abs(Dv[i])))
                Nb = Nb + 1
                box = np.absolute(Dv[i] - Dv)
                norm = np.amax(box, 1)
                pi = np.sum(norm < R) - 1 # remove ith vector (i!=j)
                # fraction of points falls in the box of center i
                p.append(pi/Nv) 
        c = (np.sum(p))/Nc
        CR.append(c)
    fit = linregress(np.log10(Rs), np.log10(CR))
    slope = [fit[0],fit[-1]]
    return Rs, CR, slope

#------------------------------------------------------------------------------
# Modified to consider data variation from 0 to 1 and not -1 to 1
def CsumNew(x, d, tau, Rs):
    Rs = np.array(Rs)
    Dv  = vector.delayVector(x, tau, d)
    Nv = len(Dv)
    Nc = int(0.1*Nv)
    CR = []
    
    for R in Rs:
        p = []
        print("\nR = ", R , " .....")
        # Selecting random centers
        iChoice = np.random.choice(np.arange(0,Nv), Nc, replace=False)
        Nb = 0
        for i in iChoice:
            # if (max(np.abs(Dv[i]))+(R/2)) < 1.0: #box inside the embe
            if (max(Dv[i]) + R) < 1 and (min(Dv[i]) - R) > 0:
                # print("Dv[i]=", Dv[i], max(np.abs(Dv[i])))
                Nb = Nb + 1
                box = np.absolute(Dv[i] - Dv)
                norm = np.amax(box, 1)
                pi = np.sum(norm < R) - 1 # remove ith vector (i!=j)
                # fraction of points falls in the box of center i
                p.append(pi/Nv) 
        cR = (np.sum(p))/Nc
        CR.append(cR)
        print("Rmin=", int(Nv*cR), "/", 10, "Rmax=", pi, "/", int(Nv/100))
        print("\nCounter=", Nb)
    fit = linregress(np.log10(Rs), np.log10(CR))
    slope = [fit[0],fit[-1]]
    return Rs, CR, slope

#------------------------------------------------------------------------------
def BoxSizeEstimator(x, tau, d):
    Dv  = vector.delayVector(x, tau, d)
    Nv = len(Dv)
    Nc = int(0.1*Nv)
    
    for R in np.arange(0.1, 0, -0.01): # Rmax
        iChoice = np.random.choice(np.arange(0,Nv), Nc, replace=False)
        Nb = 0
        for i in iChoice:
            if (max(np.abs(Dv[i]))+(R)) < 1.0: #box inside the embedded space
                Nb = Nb + 1 
        if Nb > (Nv/100):
            Rmax = R
            print("\nNb = ", Nb, " > ", int(Nv/100), "for Rmax = ", Rmax)
            break

    for R in np.arange(0.0001, 0.1, 0.0001): # Rmin
        Ns = 100
        iChoice = np.random.choice(np.arange(0,Nv), Ns, replace=False)
        pin = 0
        for i in iChoice:
            if (max(np.abs(Dv[i]))+(R)) < 1.0: #box inside the embedded space
                box = np.absolute(Dv[i] - Dv)
                norm = np.amax(box, 1)
                pi = np.sum(norm < R) - 1 # remove ith vector (i!=j)
                pin = pin + pi
        if pin/Ns > 10:
            Rmin = R
            print("Pave = ", int(pin/Ns), " > ", 10, "for Rmin = ", Rmin)
            break
        
    return Rmin, Rmax, [Nb,int(Nv/100)], [int(pin/Ns), 10]
##--------------------------------------------[Issues/Comments]----------------


